//declare module

var myApp = angular.module('myApp', [])
// create service factory
myApp.factory('myService', function () {

    var thisService = {};

    thisService.Insert = function (lista, objeto) {
        lista.push(objeto);
        return lista;
    }

    thisService.Update = function (lista, index,objeto) {
        lista[index] = objeto;
        return lista[index];
    }

    thisService.Remove = function (lista, index) {
        lista.splice(index, 1);
        return lista;
    }

    return thisService;

});
//controller
myApp.controller('ListaUsuariosController', function ($scope, myService) {

    $scope.selectedIndex = 0;
    $scope.usuario = {};
    $scope.generos = ['Masculino', 'Femenino'];
    $scope.tipoUsuarios = ['Profesional', 'Paciente'];

    $scope.usuarios = [
        { id: '00001', nombre: 'Daniel', primerApellido: 'Vivas', segundoApellido: 'Granados', genero: 'Masculino', fechaNacimiento: new Date('06/09/1992').toISOString().slice(0,10), direccion: { calle: '28', numero: '85', puerta: '36', ciudad: 'Popayan' }, tipoUsuario: 'Profesional' },
        { id: '00002', nombre: 'Fernando', primerApellido: 'Alonso', segundoApellido: 'F1', genero: 'Masculino', fechaNacimiento: new Date('07/08/1985').toISOString().slice(0,10), direccion: { calle: '134', numero: '67', puerta: '211', ciudad: 'Madrid' }, tipoUsuario: 'Paciente' },
        { id: '00003', nombre: 'Daniel', primerApellido: 'Ricciardo', segundoApellido: 'F1', genero: 'Masculino', fechaNacimiento: new Date('07/12/1990').toISOString().slice(0,10), direccion: { calle: '77', numero: '21', puerta: '34', ciudad: 'Melbourbne' }, tipoUsuario: 'Paciente' }
    ];

    $scope.addUsuario = function (formUsuario) {
        $scope.usuarios = myService.Insert($scope.usuarios, $scope.usuario);
        $scope.usuario = {};
       
    };

    $scope.editarUsuario = function (index) {
        $scope.selectedIndex = index;
        $scope.usuario = angular.copy($scope.usuarios[index]);
        $scope.edit = true;
    };

    $scope.applyChanges = function (index) {
        $scope.usuario = myService.Update($scope.usuarios, $scope.selectedIndex, $scope.usuario);
        $scope.usuario = {};
        $scope.edit = false;
    };

    $scope.deleteUsuario = function () {
        $scope.usuarios = myService.Remove($scope.usuarios, $scope.selectedIndex);
    };

    $scope.borrarMedicos = function () {
        $scope.usuarios = $scope.usuarios.filter((item) => {
            return item.tipoUsuario == 'Profesional'
        });
    }

    $scope.dateChange = function ($event) {
        $scope.usuario.fechaNacimiento = new Date($scope.usuario.fechaNacimiento).toISOString().slice(0,10);;
    }

    $scope.cancelForm = function () {
        $scope.usuario = {};
        $scope.edit = false;
    }

    // This method associate to Delete Button.
    $scope.confirmDelete = function (index) {
        $scope.selectedIndex = index;
    }


});


